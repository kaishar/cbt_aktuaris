<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cbt_user_grup_model extends CI_Model{
	public $table = 'cbt_user_grup';
	
	function __construct(){
        parent::__construct();
    }
	
    function save($data){
        $this->db->insert($this->table, $data);
    }
    
    function delete($kolom, $isi){
        $this->db->where($kolom, $isi)
                 ->delete($this->table);
    }
    
    function update($kolom, $isi, $data){
        $this->db->where($kolom, $isi)
                 ->update($this->table, $data);
    }
    
    function count_by_kolom($kolom, $isi){
        $this->db->select('COUNT(*) AS hasil')
                 ->where($kolom, $isi)
                 ->from($this->table);
        return $this->db->get();
    }
	
	function get_by_kolom($kolom, $isi){
        $this->db->where($kolom, $isi)
                 ->from($this->table);
        return $this->db->get();
    }
	
	function get_by_kolom_limit($kolom, $isi, $limit){
        $this->db->where($kolom, $isi)
                 ->from($this->table)
				 ->limit($limit);
        return $this->db->get();
    }

    function get_group(){
        $this->db->from($this->table)
                 ->order_by('grup_nama', 'ASC');
        return $this->db->get();
    }
	
	function get_datatable($start, $rows, $kolom, $isi){
		$this->db->where('('.$kolom.' LIKE "%'.$isi.'%")')
                 ->from($this->table)
				 ->order_by($kolom, 'ASC')
                 ->limit($rows, $start);
        return $this->db->get();
	}
    
    function get_datatable_count($kolom, $isi){
		$this->db->select('COUNT(*) AS hasil')
                 ->where('('.$kolom.' LIKE "%'.$isi.'%")')
                 ->from($this->table);
        return $this->db->get();
    }
    
    public function delete_byuser($user_id)
    {
        $sql = "delete from cbt_usergrup where user_id = ?";
        $result = $this->db->query($sql, array($user_id));

        return $result;
    }

    public function save_multiple_grup($data)
    {
        $result = $this->db->insert('cbt_usergrup', $data);
        return $result;
    }

    public function find_by_kolom($kolom, $isi)
    {
        $sql = "select grup_id from cbt_usergrup where ".$kolom." = ?";
        $result = $this->db->query($sql, array($isi));
        
        return $result;
    }
}