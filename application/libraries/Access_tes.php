<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* ZYA CBT
* Achmad Lutfi
* achmdlutfi@gmail.com
* achmadlutfi.wordpress.com
*/
class Access_tes{
	function __construct(){
		$this->CI =& get_instance();
		
		$this->CI->load->helper('cookie');
		$this->CI->load->model('cbt_user_model');
		
		$this->users_model =& $this->CI->cbt_user_model;
	}
	
	
	/**
	 * proses login
	 * 0 = username tak ada
	 * 1 = sukses
	 * 2 = password salah
	 * @param unknown_type $username
	 * @param unknown_type $password
	 * @return boolean
	 */
	function login($username, $password){
		$user_name = '';
		$user_firstname = '';
		$grup_nama = array();
		$grup_id = array();
		$user_password = '';
		
		$result = $this->users_model->get_by_username($username);
		//echo $this->CI->db->last_query(); exit;
		foreach($result as $row):
			$user_name = $row->user_name;
			$user_firstname = $row->user_firstname;
			$grup_nama[] = $row->grup_nama;
			$grup_id[] = $row->grup_id;
			$user_password = $row->user_password;
		endforeach;
		
		if($result){
			if($password === $user_password){
				$this->CI->session->set_userdata('cbt_tes_user_id',$user_name);
                $this->CI->session->set_userdata('cbt_tes_nama',stripslashes($user_firstname));
                $this->CI->session->set_userdata('cbt_tes_group',$grup_nama);
                $this->CI->session->set_userdata('cbt_tes_group_id',$grup_id);
				return 1;
			}else{
				return 2;
			}
		}
		return 0;
	}
	
	/**
	 * cek apakah sudah login
	 * @return boolean
	 */
	function is_login(){
		return (($this->CI->session->userdata('cbt_tes_user_id')) ? TRUE : FALSE);
	}
	
	function get_username(){
		return $this->CI->session->userdata('cbt_tes_user_id');
	}
    
    function get_nama(){
		return $this->CI->session->userdata('cbt_tes_nama');
	}
    
    function get_group(){
		return $this->CI->session->userdata('cbt_tes_group');
	}
    
    function get_group_id(){
		return $this->CI->session->userdata('cbt_tes_group_id');
	}
	
	/**
	 * logout
	 */
	function logout(){
		$this->CI->session->unset_userdata('cbt_tes_user_id');
		$this->CI->session->unset_userdata('cbt_tes_nama');
		$this->CI->session->unset_userdata('cbt_tes_group_id');
		$this->CI->session->unset_userdata('cbt_tes_group');
	}
}