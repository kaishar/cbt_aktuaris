<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		Peserta
		<small>Mapping peserta dengan grup, peserta dapat memiliki beberapa grup</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="<?php echo site_url() ?>/"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Peserta Grup Mapping</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">        
        <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
    						<div class="box-title">Informasi Peserta</div>    						
                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <div class="col-sm-4">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Username </td>
                                        <td>:&nbsp;<?=$user->user_name?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama Lengkap </td>
                                        <td>:&nbsp;<?=$user->user_firstname?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <?php echo form_open($url.'/save','id="form-peserta-grup-save"'); ?>
                        <input type="hidden" name="check" id="check" value="0">
                        <table id="table-grup" class="table table-striped table-bordered table-hover">
                            <thead class="thead-dark">
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">Kelompok</th>
                                    <th class="all text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $i = 1;
                                    $grup_checked = '';
                                    foreach($grup as $rows):
                                        if(in_array($rows->grup_id, array_column($user_grup, 'grup_id'))):
                                            $grup_checked = "checked";
                                            
                                        else:
                                            $grup_checked = '';
                                        endif;	                                           		
                                ?>
                                <tr>
                                    <td style="width: 10px;" class="text-center"><?=$i?></td>
                                    <td><?=$rows->grup_nama?></td>
                                    <td style="width: 10px;" class="text-center"><input type="checkbox" name="grup_id[]" value="<?=$rows->grup_id?>" <?=$grup_checked?>></td>									
                                </tr>
                                <?php
                                    $i++;
                                    endforeach;
                                ?>
                            </tbody>
                        </table> 
                        <input type="hidden" name="user_id" value="<?=$user->user_id?>" >
                        </form>                      
                    </div>
                    <div class="box-footer">
                        <button type="button" id="btn-save" class="btn btn-primary" title="Simpan Grup yang dipilih">Simpan</button>
                        <button type="button" id="btn-edit-pilih" class="btn btn-default pull-right">Pilih Semua</button>
                    </div>
                </div>
        </div>
    </div>
</section><!-- /.content -->

<script lang="javascript">
    $(function()
    {        
        $('#btn-save').click(function(){
            $('#form-peserta-grup-save').submit();
        });

        $('#btn-edit-pilih').click(function(event) {
            if($('#check').val()==0) {
                $(':checkbox').each(function() {
                    this.checked = true;
                });
                $('#check').val('1');
            }else{
                $(':checkbox').each(function() {
                    this.checked = false;
                });
                $('#check').val('0');
            }
        }); 

        <?php 
            if(!empty($this->session->flashdata('message'))):            
        ?>
            notify_success("<?=$this->session->flashdata('message')?>");
        <?php
            endif;
        ?>                               
    });   
    
</script>