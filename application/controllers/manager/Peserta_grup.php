<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Peserta_grup extends Member_Controller {
	private $kode_menu = 'peserta-grup';
	private $kelompok = 'peserta';
	private $url = 'manager/peserta_grup';
	
    function __construct(){
		parent:: __construct();
		$this->load->model('cbt_user_grup_model');
		$this->load->model('cbt_user_model');

		parent::cek_akses($this->kode_menu);
	}
	
    public function index($user_id=null){
		$data['kode_menu'] = $this->kode_menu;
		$data['url'] = $this->url;
		
		if(!empty($user_id))
		{
			$query_user = $this->cbt_user_model->get_by_kolom('user_id', $user_id);
			$data_user_grup = $this->cbt_user_grup_model->find_by_kolom('user_id', $user_id);
		}else {
			redirect('manager/peserta_daftar');
		}

		$query_grup = $this->cbt_user_grup_model->get_group();
				
		$data['user'] = $query_user->row();
		$data['grup'] = $query_grup->result();
		$data['user_grup'] = $data_user_grup->result_array();

        $this->template->display_admin($this->kelompok.'/peserta_grup_mapping', 'Mapping Peserta Grup', $data);
	}
	
	public function save()
	{
		$user_id = $this->input->post('user_id');
		$grups = $this->input->post('grup_id');
		
		$this->cbt_user_grup_model->delete_byuser($user_id);
		foreach($grups as $key => $value):
			$save['user_id'] = $user_id;
			$save['grup_id'] = $value;
			$result = $this->cbt_user_grup_model->save_multiple_grup($save);
		endforeach;

		$this->session->set_flashdata('message', 'Data Peserta Grup berhasil disimpan ');
		redirect('manager/peserta_grup/index/'.$user_id);
	}
}